using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Storage;
using Firebase.Extensions;
using Firebase.Firestore;
using Firebase.Analytics;
using UnityEngine.SceneManagement;

	/*Copyright (C) 2021  Reboot Rebels( Nick Salvi, Trevor Blanchard, Ryan Cote)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see https://www.gnu.org/licenses/.
	*/
public class FirebaseListener : MonoBehaviour
{

    private int sceneToChange;
    public GameObject Driver;
    private Firebase.FirebaseApp app;
    // Start is called before the first frame update
    void Start()
    {
       // DontDestroyOnLoad(Driver);
        //Mack2DoubleSided.GetComponent<Renderer>().sharedMaterial = tempMaterial;

        //do the firestore databse stuff now.
        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;

        //get a connection
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                app = Firebase.FirebaseApp.DefaultInstance;

                // Set a flag here to indicate whether Firebase is ready to use by your app.
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });

        //Debug.Log("1 complete");

        //set a reference to the spot we are reading from.
        DocumentReference docRef = db.Collection("SceneTracker").Document("Tracker");
        //create a dictionary where we going to store the info from the database and give default value.


        //Debug.Log("2 complete");

        //read from the database.
        docRef.GetSnapshotAsync().ContinueWithOnMainThread(task =>
        {
            Debug.Log("making sure!");
            DocumentSnapshot snapshot = task.Result;
            if (snapshot.Exists)
            {
                Debug.Log(string.Format("Document data for {0} document:", snapshot.Id));
                Dictionary<string, object> test = snapshot.ToDictionary();
                foreach (KeyValuePair<string, object> pair in test)
                {
                    Debug.Log(string.Format("{0}: {1}", pair.Key, pair.Value));
                    sceneToChange = System.Convert.ToInt32(pair.Value);
                }
            }
            else
            {
                Debug.Log(string.Format("Document {0} does not exist!", snapshot.Id));
            }
        });


        //Debug.Log("3 come on i know u can do it!");

        //set up listener to listen to the database.
        Debug.Log("still going!!!");
        docRef.Listen(snapshot => {
            Debug.Log("Callback received document snapshot.");
            Debug.Log(string.Format("Document data for {0} document:", snapshot.Id));
            Dictionary<string, object> test2 = snapshot.ToDictionary();
            foreach (KeyValuePair<string, object> pair in test2)
            {
                Debug.Log(string.Format("{0}: {1}", pair.Key, pair.Value));
                sceneToChange = System.Convert.ToInt32(pair.Value);
                
                if(sceneToChange != 0) /* If the scene is 0, then the play has not started, so wait on the loading screen. */
                {
                    if(sceneToChange == 5)
                    {
                        Scene currentScene = SceneManager.GetActiveScene();
                        string sceneName = currentScene.name;

                        /* This is basically a special case where a person starts the app when scene3 is not loaded, yet the storm trigger has fired, 
                         * In this case, if the current scene is not scene3, load it before setting the flag.
                         */

                        if (sceneName != "scene3") 
                        {                           
                            Driver.GetComponent<FetchScene>().beginSceneChange(3, Driver);
                        }
                        GlobalVariables.treeStormFlag = 1;
                    }
                    else
                    {
                        Driver.GetComponent<FetchScene>().beginSceneChange(sceneToChange, Driver);
                    }
                }
            }
        });
    }

}
