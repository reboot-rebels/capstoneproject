using System.Collections;
using System.Collections.Generic;
using UnityEngine;
	/*Copyright (C) 2021  Reboot Rebels( Nick Salvi, Trevor Blanchard, Ryan Cote)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see https://www.gnu.org/licenses/.
	*/
public class TreeScale : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject tree;
    public GameObject storm;
    public GameObject sceneStorm;
    public GameObject overlayCanvas;

    private ParticleSystem partSystemTree;
    private ParticleSystem partSystemScene;
    private CanvasGroup overlay;
    private int size = 0;
    private int maxIterations = 400;
    private float stormRate = 10.0f;
    private float stormRateScene = 10.0f;
    private float whiteCanvasAlpha = 0.0f;

    bool stopScaleTree = false; 
    bool stopFadeToWhite = false;
    bool stopUnFadeToWhite = false;

    private Vector3 scaleVector = new Vector3(0.007f, 0.007f, 0.007f);


    void Start()
    {
        partSystemTree = storm.GetComponent<ParticleSystem>();
        partSystemScene = sceneStorm.GetComponent<ParticleSystem>();
        overlay = overlayCanvas.GetComponent<CanvasGroup>();

        InvokeRepeating("scaleTree", 0.0f, 0.03f);

    }

    private void scaleTree()
    {
        
        tree.transform.localScale += scaleVector;
        increaseStormIntensity();
        Debug.Log(size);
        size++;
        if (size >= maxIterations-1)
        {
            stopScaleTree = true;
        }
    }

    private void fadeToWhite()
    {
        whiteCanvasAlpha += 0.01f;
        overlay.alpha = whiteCanvasAlpha;
        size++;
        if(size >= 2*maxIterations-1)
        {
            stopFadeToWhite = true;
            whiteCanvasAlpha = 1;
        }
    }
    private void unfadeToWhite()
    {
        whiteCanvasAlpha += -0.02f;
        overlay.alpha = whiteCanvasAlpha;
        size++;
        if (size >= 3*maxIterations-1)
        {
            stopUnFadeToWhite = true;
        }
    }
    private void increaseStormIntensity()
    {
        var emission = partSystemTree.emission;
        var emissionScene = partSystemScene.emission;
        stormRate = stormRate + 6.0f;
        stormRateScene = stormRateScene + 0.2f;
        emission.rateOverTime = stormRate;
        emissionScene.rateOverTime = stormRateScene;
    }
    // Update is called once per frame
    void Update()
    {
        if(stopScaleTree)
        {
            CancelInvoke("scaleTree");
            Debug.Log("Cancelled");
            stopScaleTree = false;
            InvokeRepeating("fadeToWhite", 0.0f, 0.03f);
        }

        if (stopFadeToWhite)
        {
            tree.SetActive(false);
            CancelInvoke("fadeToWhite");
            Debug.Log("Cancelled2");
            stopFadeToWhite = false;
            InvokeRepeating("unfadeToWhite", 0.0f, 0.03f);
        }

        if (stopUnFadeToWhite)
        {
            CancelInvoke("unfadeToWhite");
            Debug.Log("Cancelled3");
            stopUnFadeToWhite = false;
        }
    }
}
