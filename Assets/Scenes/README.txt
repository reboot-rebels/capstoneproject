For scenes there is some critical informations:

IOS- The directional light objects MUST be ticked to "Important" for their render mode, otherwise the shader will not build.


Android- The directional light objects MUST be ticked to "Auto" for their render mode, otherwise the shader will not build.

For both - The assetbundles must be build for the target platform; meaning the IOS bundles must be built while the editor is on the IOS platform.