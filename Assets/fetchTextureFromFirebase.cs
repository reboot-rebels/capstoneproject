using Firebase;
using Firebase.Database;
using Firebase.Storage;
using Firebase.Extensions;
using Firebase.Firestore;
using Firebase.Analytics;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

	/*Copyright (C) 2021  Reboot Rebels( Nick Salvi, Trevor Blanchard, Ryan Cote)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see https://www.gnu.org/licenses/.
	*/
public class fetchTextureFromFirebase : MonoBehaviour
{
    public Material pineMaterial;
    public Material barkMaterial;
    public Material iglooMaterial;


    public Material xmasStarMaterial;
    public Material xmasLightsMaterial;
    public Material xmasOrbsMaterial;
    public Material xmasBranch1Material;
    public Material xmasBranch2Material;
    public ARSessionOrigin pos;


    Firebase.Storage.FirebaseStorage storage;
    Firebase.Storage.StorageReference storage_ref;
    // Start is called before the first frame update
    void Start()
    {
        storage = Firebase.Storage.FirebaseStorage.DefaultInstance;
        storage_ref = storage.GetReferenceFromUrl("gs://rebootrebelsar-7ba54.appspot.com/BarkMaterials");

        if ( GlobalVariables.seatPosition == 1)
        {
            pos.transform.position = new Vector3(3.0f, 1.5f, 1.5f);
        }
        else if(GlobalVariables.seatPosition == 3)
        {
            pos.transform.position = new Vector3(-3.0f, 1.5f, 1.5f);
        }
        else
        {
            pos.transform.position = new Vector3(0, 1.5f, 1.5f);
        }

        //Fetch tree branch textures
        fetchTexture("branches.png", "_MainTex", pineMaterial, 2048, 2048);

        //fetch the bark maps
        fetchTexture("bark_basecolor.png", "_MainTex" , barkMaterial, 2048, 1024);
        fetchTexture("bark_metallic.png", "_Metallic", barkMaterial, 2048, 1024);
        fetchTexture("bark_normal.png", "_BumpMap", barkMaterial, 2048, 1024);
        fetchTexture("bark_height.png", "_ParallaxMap", barkMaterial, 2048, 1024);

        //fetch igloo materials
        fetchTexture("m_igloo_Base_Color.png", "_MainTex", iglooMaterial, 2048, 2048);
        fetchTexture("m_igloo_Metallic.png", "_Metallic", iglooMaterial, 2048, 2048);
        fetchTexture("m_igloo_Mixed_AO.png", "_OcclusionMap", iglooMaterial, 2048, 2048);
        fetchTexture("m_igloo_Normal_OpenGL.png", "_BumpMap", iglooMaterial, 2048, 2048);

        //fetch xmas star
        //fetchTexture("Weihnachtsbaum_BaseColor.lambert6.png", "_MainTex", xmasStarMaterial);
       // fetchTexture("Weihnachtsbaum_Normal.lambert6.png", "_BumpMap", xmasStarMaterial);

        fetchTexture("lichter.png", "_MainTex", xmasLightsMaterial, 512, 512);

        //fetchTexture("Weihnachtsbaum_BaseColor.lambert1.png", "_MainTex", xmasOrbsMaterial);
       // fetchTexture("Weihnachtsbaum_Normal.lambert1.png", "_BumpMap", xmasOrbsMaterial);

        fetchTexture("purepng.com-fir-treetreefirsabiestree-fami.png", "_MainTex", xmasBranch1Material, 1024, 1024);
        fetchTexture("purepng.com-fir-treetreefirsabiestree-fami.png", "_MainTex", xmasBranch2Material, 1024, 1024);


    }


    void fetchTexture(string textureName, string textureSlotName, Material mat, int fileSize, int textureSize)
    {
            byte[] fileContents = null;
            storage_ref.Child(textureName).GetBytesAsync(fileSize * fileSize)
                 .ContinueWithOnMainThread((Task<byte[]> task) =>
                 {
                     if (task.IsFaulted || task.IsCanceled)
                     {
                         Debug.Log(task.Exception.ToString());
                     }
                     else
                     {
                         byte[] fileContents = task.Result;
                         Debug.Log("Load Image after getting result!");
                         Texture2D texture = new Texture2D(textureSize, textureSize);
                         texture.LoadImage(fileContents);
                         mat.SetTexture(textureSlotName, texture);                    
                     }
                 });
    }
}
