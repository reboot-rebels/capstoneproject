using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

	/*Copyright (C) 2021  Reboot Rebels( Nick Salvi, Trevor Blanchard, Ryan Cote)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see https://www.gnu.org/licenses/.
	*/
public class StaticNoiseOffset : MonoBehaviour
{
    public GameObject image;
    Material mat;
    float rand;

    // Start is called before the first frame update
    void Start()
    {
        mat = image.GetComponent<Image>().material;
    }

    // Update is called once per frame
    void Update()
    {
        rand = Random.Range(0.0f, 1.0f);
        mat.SetTextureOffset("_MainTex", new Vector2(rand, rand));
    }
}
